{H:04##request_InstdDrctPty##DCPS##request_InstgDrctPty##DCPS##method_date(yyyyMMddHHmmss)##XMLdcep.934.001.01     ##method_date(yyyyMMdd)####method_seq(28)##                                            3D                                       }
<?xml version="1.0" encoding="UTF-8"?>

<Document xmlns="urn:cbcc:std:dcep:2020:tech:xsd:dcep.934.001.01" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<LoginRspn>
		<GrpHdr>
			<MsgId>##method_date(yyyyMMddHHmmss)####method_seq(14)##</MsgId>
			<CreDtTm>##method_date(yyyy-MM-dd)##T##method_date(HH:mm:ss)##</CreDtTm>
			<InstgPty>
				<InstgDrctPty>##request_InstdDrctPty##</InstgDrctPty>
			</InstgPty>
			<InstdPty>
				<InstdDrctPty>##request_InstgDrctPty##</InstdDrctPty>
			</InstdPty>
			<Rmk>测试</Rmk>
		</GrpHdr>
		<OrgnlGrpInf>
			<OrgnlMsgId>##request_MsgId##</OrgnlMsgId>
		</OrgnlGrpInf>
		<LoginRspnInf>
			<LoginOprTp>##request_LoginOprTp##</LoginOprTp>
			<PrcSts>PR00</PrcSts>
		</LoginRspnInf>
	</LoginRspn>
</Document>

