{H:04##request_InstdDrctPty##DCPS##request_InstgDrctPty##DCPS##method_date(yyyyMMddHHmmss)##XMLdcep.412.001.01     ##method_date(yyyyMMdd)####method_seq(28)##                                            3D                                       }
<?xml version="1.0" encoding="UTF-8"?>

<Document xmlns="urn:cbcc:std:dcep:2020:tech:xsd:dcep.412.001.01" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<TxStsQryRsp>
		<GrpHdr>
			<MsgId>##method_date(yyyyMMddHHmmss)####method_seq(14)##</MsgId>
			<CreDtTm>##method_date(yyyy-MM-dd)##T##method_date(HH:mm:ss)##</CreDtTm>
			<InstgPty>
				<InstgDrctPty>##request_InstdDrctPty##</InstgDrctPty>
			</InstgPty>
			<InstdPty>
				<InstdDrctPty>##request_InstgDrctPty##</InstdDrctPty>
			</InstdPty>
			<Rmk>测试</Rmk>
		</GrpHdr>
		<BizQryRef>
			<QryRef>##request_MsgId##</QryRef>
			<QryNm>##request_InstgDrctPty##</QryNm>
			<QryRs>PR00</QryRs>
		</BizQryRef>
		<BizRpt>
			<TrnRs>PR00</TrnRs>
			<Rsn>
				<PrcCd>DCEPI0000</PrcCd>
			</Rsn>
			<OrgnlTxInf>
				<OrgnlMsgId>##request_OrgnlMsgId##</OrgnlMsgId>
				<OrgnlInstgPty>##request_OrgnlInstgPty##</OrgnlInstgPty>
				<OrgnlMT>##request_OrgnlMT##</OrgnlMT>
				<OrgnlBizTp>##request_OrgnlBizTp##</OrgnlBizTp>
				<OrgnlBizKind>##request_OrgnlBizKind##</OrgnlBizKind>
				<OrgnlBatchId>B##method_date(yyyyMMddHH)##00</OrgnlBatchId>
			</OrgnlTxInf>
		</BizRpt>
	</TxStsQryRsp>
</Document>