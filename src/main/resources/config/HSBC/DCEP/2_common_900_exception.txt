{H:04##request_InstdDrctPty##DCPS##request_InstgDrctPty##DCPS##method_date(yyyyMMddHHmmss)##XMLdcep.900.001.01     ##method_date(yyyyMMdd)####method_seq(28)##                                            3D                                       }
<?xml version="1.0" encoding="UTF-8"?>

<Document xmlns="urn:cbcc:std:dcep:2020:tech:xsd:dcep.900.001.01" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<CmonConf>
		<GrpHdr>
			<MsgId>##method_date(yyyyMMddHHmmss)####method_seq(14)##</MsgId>
			<CreDtTm>##method_date(yyyy-MM-dd)##T##method_date(HH:mm:ss)##</CreDtTm>
			<InstgPty>
				<InstgDrctPty>##request_InstdDrctPty##</InstgDrctPty>
			</InstgPty>
			<InstdPty>
				<InstdDrctPty>##request_InstgDrctPty##</InstdDrctPty>
			</InstdPty>
			<Rmk>测试</Rmk>
		</GrpHdr>
		<OrgnlGrpHdr>
			<OrgnlMsgId>##request_MsgId##</OrgnlMsgId>
			<OrgnlInstgPty>##request_InstgDrctPty##</OrgnlInstgPty>
			<OrgnlMT>##request_xsd##</OrgnlMT>
		</OrgnlGrpHdr>
		<CmonConfInf>
			<PrcSts>PR02</PrcSts>
			<BatchId>B##method_date(yyyyMMddHH)##00</BatchId>
		</CmonConfInf>
	</CmonConf>
</Document>
