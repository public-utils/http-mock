package com.jxx.mock.http.util;

import com.jxx.mock.http.enums.ErrorCodeEnum;
import com.jxx.mock.http.exception.CommonSystemServiceException;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * 枚举工具类
 *
 * @author xingxing jiang
 * @create 2021/06/28 11:10
 */
@Slf4j
public class EnumUtils {
    private EnumUtils() {
    }

    /**
     * 根据枚举值获取枚举
     *
     * @param enumClass 枚举类
     * @param code      枚举值
     * @param method    取值方法
     * @param <E>       枚举类型
     * @return 对应枚举
     */
    public static <E extends Enum<?>> E valueOf(Class<E> enumClass, Object code, Method method) {
        for (E e : enumClass.getEnumConstants()) {
            Object enumCode;
            try {
                method.setAccessible(true);
                enumCode = method.invoke(e);
            } catch (IllegalAccessException | InvocationTargetException e1) {
                log.error("Error: NoSuchMethod in {}.  Cause: {}", e, enumClass.getName());
                throw new CommonSystemServiceException(ErrorCodeEnum.ERROR_INVOKE_METHOD, "Error: NoSuchMethod");
            }
            if (code instanceof Number && enumCode instanceof Number
                    && new BigDecimal(String.valueOf(code)).compareTo(new BigDecimal(String.valueOf(enumCode))) == 0) {
                return e;
            }
            if (Objects.equals(enumCode, code)) {
                return e;
            }
        }
        return null;
    }

    /**
     * 根据枚举值获取枚举
     *
     * @param enumClass 枚举类
     * @param code      枚举值
     * @param <E>       枚举类型
     * @return 对应枚举
     */
    public static <E extends Enum<?>> E valueOf(Class<E> enumClass, Object code) {
        try {
            return valueOf(enumClass, code, enumClass.getMethod("getCode"));
        } catch (NoSuchMethodException e) {
            log.error("Error: NoSuchMethod in {}.  Cause: {}", e, enumClass.getName());
            throw new CommonSystemServiceException(ErrorCodeEnum.ERROR_INVOKE_METHOD, "Error: NoSuchMethod");
        }
    }

    /**
     * 根据枚举名称获取枚举
     *
     * @param enumClass  枚举类
     * @param name       枚举名称
     * @param ignoreCase 忽略大小写
     * @param <E>        枚举类型
     * @return 对应枚举
     */
    public static <E extends Enum<?>> E nameOf(Class<E> enumClass, String name, boolean ignoreCase) {
        for (E e : enumClass.getEnumConstants()) {
            if ((ignoreCase && e.name().equalsIgnoreCase(name))
                    || (!ignoreCase && e.name().equals(name))) {
                return e;
            }
        }
        return null;
    }

    /**
     * 根据枚举名称获取枚举
     *
     * @param enumClass 枚举类
     * @param name      枚举名称
     * @param <E>       枚举类型
     * @return 对应枚举
     */
    public static <E extends Enum<?>> E nameOf(Class<E> enumClass, String name) {
        return nameOf(enumClass, name, true);
    }
}