package com.jxx.mock.http.util;

import com.google.common.collect.Maps;

import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 正则工具类
 *
 * @author xingxing jiang
 * @create 2021/06/29 15:38
 */
public class PatternUtils {
    private PatternUtils() {
    }

    /**
     * 根据正则获取配置内容
     *
     * @param patternStr 正则表达式
     * @param srcStr     匹配字符串
     * @param groupName  分组名称
     * @return
     */
    public static String getValue(String patternStr, String srcStr, String groupName) {
        if (StringUtils.isEmpty(patternStr) || StringUtils.isEmpty(srcStr)) {
            return null;
        }
        Pattern pattern = Pattern.compile(patternStr);
        Matcher matcher = pattern.matcher(srcStr);
        String matcherStr = "";
        if (matcher.find()) {
            if (StringUtils.isEmpty(groupName)) {
                matcherStr = matcher.group();
            } else {
                matcherStr =  matcher.group(groupName);
            }
        }
        return StringUtils.getRealStr(matcherStr);
    }

    /**
     * 根据正则获取多个匹配内容集合
     *
     * @param patternStr 正则表达式
     * @param srcStr     匹配字符串
     * @param groupName  分组名称
     * @return
     */
    public static Map<String, String> getMatcherMap(String patternStr, String srcStr, String groupName) {
        Map<String, String> result = Maps.newConcurrentMap();
        if (StringUtils.isEmpty(patternStr) || StringUtils.isEmpty(srcStr)) {
            return result;
        }
        Pattern pattern = Pattern.compile(patternStr);
        Matcher matcher = pattern.matcher(srcStr);
        while (matcher.find()) {
            if (StringUtils.isEmpty(groupName)) {
                result.put(StringUtils.getRealStr(matcher.group(1)), matcher.group());
            } else {
                result.put(StringUtils.getRealStr(matcher.group(groupName)), matcher.group());
            }
        }
        return result;
    }

    /**
     * 根据正则获取多个匹配内容集合
     *
     * @param patternMap 正则表达式集合
     * @param srcStr     匹配字符串
     * @param groupName  分组名称
     * @return
     */
    public static Map<String, String> getMatcherValueMap(Map<String, String> patternMap, String srcStr, String groupName) {
        Map<String, String> result = Maps.newConcurrentMap();
        if (Objects.isNull(patternMap) || StringUtils.isEmpty(srcStr)) {
            return result;
        }
        patternMap.forEach((key, value) -> {
            Pattern pattern = Pattern.compile(value);
            Matcher matcher = pattern.matcher(srcStr);
            if (matcher.find()) {
                if (StringUtils.isEmpty(groupName)) {
                    result.put(key, StringUtils.getRealStr(matcher.group(1)));
                } else {
                    result.put(key, StringUtils.getRealStr(matcher.group(groupName)));
                }
            } else {
                pattern = Pattern.compile(value.substring(0, value.length() - 10));
                matcher = pattern.matcher(srcStr);
                if (matcher.find()) {
                    if (StringUtils.isEmpty(groupName)) {
                        result.put(key, StringUtils.getRealStr(matcher.group(1)));
                    } else {
                        result.put(key, StringUtils.getRealStr(matcher.group(groupName)));
                    }
                }
            }
        });
        return result;
    }


    public static void main(String[] args) {
        String str1 = "name= jxx jiang & age=18";
        String str2 = "name : \"jxx jiang\" ,age:18";
        String str3 = "<name> jxx jiang </name><age>18</age>";
        String patternStr = "age[ &=:<,>\"]+(?<replaceKey>.*?)[&=:<,>\"]+";
        System.out.println(patternStr.substring(0, patternStr.length() - 10));
        Pattern compile = Pattern.compile(patternStr);
        Matcher matcher = compile.matcher(str1);
        if (matcher.find()) {
            System.out.println("======1=====" + matcher.group("replaceKey"));
        }
        matcher = compile.matcher(str2);
        if (matcher.find()) {
            System.out.println("======2=====" + matcher.group("replaceKey"));
        }
        matcher = compile.matcher(str3);
        if (matcher.find()) {
            System.out.println("======3=====" + matcher.group("replaceKey"));
        }
    }
}