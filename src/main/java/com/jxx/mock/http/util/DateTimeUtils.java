package com.jxx.mock.http.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * 时间工具
 *
 * @author xingxing jiang
 * @create 2021/06/21 9:17
 */
public final class DateTimeUtils {
    private DateTimeUtils() {
    }

    private static final DateTimeFormatter DEFAULT_FULL_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    private static final DateTimeFormatter DEFAULT_FORMATTER = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
    private static final DateTimeFormatter SHORT_DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyyMMdd");


    /**
     * 获取当前日期的yyyyMMddHHmmss格式
     *
     * @return
     */
    public static String defaultDatetime() {
        return LocalDateTime.now().format(DEFAULT_FORMATTER);
    }


    /**
     * 获取当前日期的yyyy-MM-dd HH:mm:ss格式
     *
     * @return
     */
    public static String defaultFullDatetime() {
        return LocalDateTime.now().format(DEFAULT_FULL_FORMATTER);
    }

    /**
     * 获取当前日期的yyyyMMdd格式
     *
     * @return
     */
    public static String shortDate() {
        return LocalDate.now().format(SHORT_DATE_FORMATTER);
    }

    public static String formatDatetime(String format) {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern(format));
    }
}