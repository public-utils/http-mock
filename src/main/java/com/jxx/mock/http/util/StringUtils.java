package com.jxx.mock.http.util;

import java.util.Map;
import java.util.Objects;

/**
 * 字符串工具类
 *
 * @author xingxing jiang
 * @create 2021/06/29 12:10
 */
public class StringUtils {
    private StringUtils() {
    }


    /**
     * 判断字符串是否为空或null
     *
     * @param value 入参字符串
     * @return 是否为空
     */
    public static boolean isEmpty(String value) {
        return null == value || "".equals(value.trim());
    }

    /**
     * 判断字符串是否非空
     *
     * @param value 入参字符串
     * @return 是否非空
     */
    public static boolean isNotEmpty(String value) {
        return !isEmpty(value);
    }

    /**
     * 去掉字符串两边双引号和空格
     *
     * @param value 入参字符串
     * @return 是否非空
     */
    public static String getRealStr(String value) {
        if (isEmpty(value)) {
            return "";
        }
        String trim = value.trim();
        int startIndex = 0;
        int endIndex = trim.length();
        if (trim.startsWith("\"")) {
            startIndex += 1;
        }
        if (trim.endsWith("\"")) {
            endIndex -= 1;
        }
        return trim.substring(startIndex, endIndex).trim();
    }

    /**
     * 连接字符串
     *
     * @param objs 入参数组
     * @return
     */
    public static String join(Object... objs) {
        return joinWith("", objs);
    }

    /**
     * 连接字符串
     *
     * @param connector 连接符
     * @param objs      入参数组
     * @return
     */
    public static String joinWith(String connector, Object... objs) {
        if (Objects.isNull(objs)) {
            return null;
        }
        if (StringUtils.isEmpty(connector)) {
            connector = "";
        }
        StringBuffer sb = new StringBuffer();
        for (Object obj : objs) {
            sb.append(connector).append(obj);
        }
        return sb.substring(connector.length());
    }

    /**
     * 在srcStr中，将replaceMap中映射替换
     *
     * @param srcStr     源字符串
     * @param replaceMap 替换映射map
     * @return
     */
    public static String replaceMap(String srcStr, Map<String, String> replaceMap) {
        if (isEmpty(srcStr) || Objects.isNull(replaceMap)) {
            return srcStr;
        }
        for (Map.Entry<String, String> entry : replaceMap.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            srcStr = srcStr.replace(key, value);
        }
        return srcStr;
    }

    /**
     * 生成定长字符串
     *
     * @param srcStr    源字符串
     * @param length    定长长度
     * @param fillStr   填充字符
     * @param direction 方向  1：左补，其他右补
     * @return
     */
    public static String fixedLength(String srcStr, int length, String fillStr, int direction) {
        if (isEmpty(srcStr)) {
            srcStr = "";
        }
        int srcLength = srcStr.length();
        if (length <= srcStr.length()) {
            return srcStr;
        }
        if (isEmpty(fillStr)) {
            fillStr = " ";
        }
        int fillLength = fillStr.length();
        int size = (length - srcLength) / fillLength;
        StringBuffer sb = new StringBuffer(length - srcLength);
        for (int i = 0; i < size; i++) {
            sb.append(fillStr);
        }
        if (1 != direction) {
            return sb.insert(0, srcStr).toString();
        }
        return sb.append(srcStr).toString();
    }
}