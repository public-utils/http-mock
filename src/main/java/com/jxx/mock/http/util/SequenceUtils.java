package com.jxx.mock.http.util;

import com.jxx.mock.http.util.help.SnowflakeId;

/**
 * 序列号生成工具类
 *
 * @author xingxing jiang
 * @create 2021/06/29 12:09
 */
public class SequenceUtils {
    private SequenceUtils() {
    }

    /**
     * 雪花算法实例
     */
    private static final SnowflakeId SNOWFLAKE_ID = new SnowflakeId(1, 1);

    /**
     * 生成流水号
     *
     * @return
     */
    public static String flowNo() {
        return StringUtils.join(DateTimeUtils.shortDate(), SNOWFLAKE_ID.nextId());
    }

    /**
     * 生成定长序列
     *
     * @param length
     * @return
     */
    public static String generateSeq(int length) {
        if (length <= 0) {
            return null;
        }
        if (length < 18) {
            return String.valueOf(SNOWFLAKE_ID.nextId()).substring(19 - length);
        }
        return StringUtils.fixedLength(String.valueOf(SNOWFLAKE_ID.nextId()), length, "0", 1);

    }
}