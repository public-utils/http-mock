/**
 * Bestpay.com.cn Inc.
 * Copyright (c) 2011-2018 All Rights Reserved.
 */
package com.jxx.mock.http.netty;

import com.jxx.mock.http.netty.handler.HttpRequestHandler;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * netty server
 *
 * @author jiang
 * @version Id: NettyServer.java, v 0.1 2018/7/9 14:41 jiang Exp $$
 */
@Slf4j
@Component
public class NettyServer {
    @Value("${netty.port:9091}")
    private int port;
    private final HttpRequestHandler httpServerHandler;

    public NettyServer(HttpRequestHandler httpServerHandler) {
        this.httpServerHandler = httpServerHandler;
    }

    @PostConstruct
    public void start() {
        ServerBootstrap bootstrap = new ServerBootstrap();
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        EventLoopGroup workerGroup = new NioEventLoopGroup();

        bootstrap.group(bossGroup, workerGroup);
        bootstrap.channel(NioServerSocketChannel.class);
        //channel的属性配置
        bootstrap.childOption(ChannelOption.TCP_NODELAY, true);//是否使用fullRequest fullResponse发送数据
        bootstrap.childOption(ChannelOption.SO_REUSEADDR, true);  //是否允许端口占用
        bootstrap.childOption(ChannelOption.SO_KEEPALIVE, false);//是否设置长连接
        bootstrap.childOption(ChannelOption.SO_RCVBUF, 2048); //设置接收数据大小
        bootstrap.childOption(ChannelOption.SO_SNDBUF, 2048);//设置发送数据大小
        bootstrap.childHandler(new ChannelInitializer<SocketChannel>() {
            @Override
            public void initChannel(SocketChannel ch) {
                ch.pipeline().addLast("codec", new HttpServerCodec());//http编解码器
                //对http msg进行聚合 转化为fullHttpRequest 或者fullHttpResponse并设置最大数据长度
                ch.pipeline().addLast("aggregator", new HttpObjectAggregator(512 * 1024));
                ch.pipeline().addLast("handler", httpServerHandler); //请求匹配处理
            }
        });
        ChannelFuture channelFuture = bootstrap.bind(port).syncUninterruptibly()
                .addListener(future -> log.info("同步接口模拟器服务启动。。。。。。。。。。"));

        channelFuture.channel().closeFuture().addListener(future -> {
            log.info("同步接口模拟器服务关闭。。。。。。。。。。");
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        });
    }
}