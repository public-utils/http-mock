package com.jxx.mock.http.netty.handler;

import com.google.common.collect.Maps;
import com.jxx.mock.http.config.ResponseContentConfig;
import com.jxx.mock.http.config.entity.*;
import com.jxx.mock.http.constant.AppConstant;
import com.jxx.mock.http.constant.GroupName;
import com.jxx.mock.http.entity.po.ReqRegisterPo;
import com.jxx.mock.http.enums.*;
import com.jxx.mock.http.exception.CommonSystemServiceException;
import com.jxx.mock.http.service.IReqRegisterService;
import com.jxx.mock.http.service.localmethod.ILocalMethod;
import com.jxx.mock.http.util.EnumUtils;
import com.jxx.mock.http.util.PatternUtils;
import com.jxx.mock.http.util.SequenceUtils;
import com.jxx.mock.http.util.StringUtils;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.*;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author xingxing jiang
 * @create 2021/06/29 11:25
 */
@Slf4j
@Component
@ChannelHandler.Sharable
public class HttpRequestHandler extends SimpleChannelInboundHandler<FullHttpRequest> {
    private final ResponseContentConfig config;
    private final IReqRegisterService reqRegisterService;
    private final static String PLACEHOLDER = "<strPath>";

    public HttpRequestHandler(ResponseContentConfig config,
                              @Qualifier("reqRegisterServiceQueue") IReqRegisterService reqRegisterService) {
        this.config = config;
        this.reqRegisterService = reqRegisterService;
    }

    @Override
    protected void messageReceived(ChannelHandlerContext channelHandlerContext, FullHttpRequest fullHttpRequest) throws Exception {
        FullHttpResponse fullHttpResponse = null;
        ReqRegisterPo registerPo = ReqRegisterPo.builder().build();
        try {
            String flowNo = SequenceUtils.flowNo();
            MDC.put(AppConstant.LOG_SEQ, flowNo);
            HttpMethodEnum method = EnumUtils.nameOf(HttpMethodEnum.class, fullHttpRequest.method().toString());
            ByteBuf content = fullHttpRequest.content();
            byte[] reqByte = new byte[content.readableBytes()];
            content.readBytes(reqByte);
            String reqStr = new String(reqByte, "UTF-8");
            registerPo.setUri(fullHttpRequest.uri());
            registerPo.setHttpMethod(method.getCode());
            registerPo.setOrderId(flowNo);
            registerPo.setReqPram(reqStr);
            switch (method) {
                case GET:
                case POST:
                    log.info("请求uri:[{}],请求方法[{}],请求参数[{}]",
                            fullHttpRequest.uri(),
                            method.name(),
                            reqStr);
                    ByteBuf buf = handlerRequest(fullHttpRequest, reqStr, registerPo);
                    fullHttpResponse = response(HttpResponseStatus.OK, buf);
                    registerPo.setRespCode(HttpResponseStatusEnum.SUCCESS.getCode());
                    registerPo.setRespMsg(HttpResponseStatusEnum.SUCCESS.getMsg());
                    log.info("请求uri:[{}],请求方法[{}],响应状态码[{}]",
                            fullHttpRequest.uri(),
                            fullHttpRequest.method().toString(),
                            HttpResponseStatus.OK);
                    break;
                case DELETE:
                case HEAD:
                case CONNECT:
                case OPTIONS:
                case PATCH:
                case TRACE:
                    log.warn("请求uri:[{}],不支持的http请求方法[{}]", fullHttpRequest.uri(), method.name());
                    fullHttpResponse = response(HttpResponseStatus.METHOD_NOT_ALLOWED, null);
                    log.info("请求uri:[{}],响应状态码[{}]", fullHttpRequest.uri(), HttpResponseStatus.METHOD_NOT_ALLOWED);
                    registerPo.setRespCode(HttpResponseStatusEnum.NO_SUPPORT_METHOD.getCode());
                    registerPo.setRespMsg(HttpResponseStatusEnum.NO_SUPPORT_METHOD.getMsg());
                    break;
            }
        } catch (Exception e) {
            log.warn("请求uri:[{}],服务端异常", fullHttpRequest.uri(), e);
            fullHttpResponse = response(HttpResponseStatus.INTERNAL_SERVER_ERROR, null);
            log.info("请求uri:[{}],响应状态码[{}]", fullHttpRequest.uri(), HttpResponseStatus.INTERNAL_SERVER_ERROR);
            registerPo.setRespCode(HttpResponseStatusEnum.INTERNAL_EXCEPTION.getCode());
            registerPo.setRespMsg(HttpResponseStatusEnum.INTERNAL_EXCEPTION.getMsg());
        } finally {
            reqRegisterService.insert(registerPo);
            if (Objects.isNull(fullHttpResponse)) {
                fullHttpResponse = response(HttpResponseStatus.INTERNAL_SERVER_ERROR, null);
            }
        }
        channelHandlerContext.writeAndFlush(fullHttpResponse).addListener(ChannelFutureListener.CLOSE);
    }

    private FullHttpResponse response(HttpResponseStatus status, ByteBuf buf) throws JSONException {
        if (Objects.isNull(status)) {
            status = HttpResponseStatus.INTERNAL_SERVER_ERROR;
        }
        if (Objects.isNull(buf)) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("code", status.code());
            jsonObject.put("msg", status.reasonPhrase());
            buf = Unpooled.wrappedBuffer(jsonObject.toString().getBytes(StandardCharsets.UTF_8));
        }
        FullHttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_0,
                status, buf);
        response.headers()
                .set(HttpHeaderNames.CONTENT_TYPE, "application/json;charset=UTF-8")
                .set(HttpHeaderNames.CONTENT_LENGTH, String.valueOf(response.content().readableBytes()));
        return response;
    }

    private ByteBuf handlerRequest(FullHttpRequest fullHttpRequest, String reqStr, ReqRegisterPo registerPo) throws UnsupportedEncodingException {
        Response response = null;
        String responseContent;
        try {
            String uriPath = fullHttpRequest.uri();
            URI uri = config.getURI(uriPath);
            response = getResponse(uri, uriPath, reqStr, registerPo);
            responseContent = getResponseContent(uriPath, reqStr, response, registerPo);
        } catch (Exception e) {
            log.error("this is a error.", e);
            if (Objects.nonNull(response)) {
                responseContent = response.getContent();
            } else {
                throw e;
            }
        }
        return Unpooled.wrappedBuffer(responseContent.getBytes());
    }

    private String getResponseContent(String uriPath, String reqStr, Response response, ReqRegisterPo registerPo) {
        String content = response.getContent();
        if (StringUtils.isEmpty(content)) {
            throw new CommonSystemServiceException(ErrorCodeEnum.ERROR_RESPONSE_CONFIG, "响应配置有误");
        }
        if (response.isModel()) {
            if (StringUtils.isEmpty(uriPath) && StringUtils.isEmpty(reqStr)) {
                throw new CommonSystemServiceException(ErrorCodeEnum.ERROR_REQ_PARAM, "请求参数有误");
            }
            //替换uri变量
            content = replaceAllModel(uriPath, content, PatternEnum.URI);
            //替换request变量
            content = replaceAllModel(reqStr, content, PatternEnum.REQUEST);
            //替换本地方法变量
            content = replaceAllModel(uriPath, content, PatternEnum.METHOD);
        }
        registerPo.setRespPram(content);
        return content;
    }

    private String replaceAllModel(String srcStr, String content, PatternEnum patternEnum) {
        Map<String, String> allValueMap = null;
        //1.先获取所有占位符集合
        Map<String, String> allPlaceholderMap = null;
        String patternStr = new StringBuffer("##")
                .append(PLACEHOLDER)
                .append("_(?<")
                .append(GroupName.REPLACE_KEY)
                .append(">.*?)##").toString();
        switch (patternEnum) {
            case METHOD:
                patternStr = patternStr.replace(PLACEHOLDER, PatternEnum.METHOD.name().toLowerCase());
                break;
            case HEADER:
                patternStr = patternStr.replace(PLACEHOLDER, PatternEnum.HEADER.name().toLowerCase());
                break;
            case REQUEST:
                patternStr = patternStr.replace(PLACEHOLDER, PatternEnum.REQUEST.name().toLowerCase());
                break;
            case URI:
                patternStr = patternStr.replace(PLACEHOLDER, PatternEnum.URI.name().toLowerCase());
                break;
            default:
                throw new CommonSystemServiceException(ErrorCodeEnum.ERROR_RESPONSE_CONFIG, "响应配置有误");
        }
        allPlaceholderMap = PatternUtils.getMatcherMap(patternStr, content, GroupName.REPLACE_KEY);
        if (Objects.isNull(allPlaceholderMap)) {
            throw new CommonSystemServiceException(ErrorCodeEnum.ERROR_REQ_PARAM, "请求参数有误");
        }
        if (allPlaceholderMap.size() == 0) {
            return content;
        }

        //2.获取所有占位符的值集合
        if (patternStr.equals("##method_(?<replaceKey>.*?)##")) {
            allValueMap = Maps.newConcurrentMap();
            for (Map.Entry<String, String> entry : allPlaceholderMap.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                String replaceValue = getLocalMethodValue(key);
                allValueMap.put(value, replaceValue);
            }
        } else {
            String valuePatternModel = new StringBuffer(PLACEHOLDER)
                    .append("[ &=:<,>\"]+(?<")
                    .append(GroupName.REPLACE_KEY)
                    .append(">.*?)[&=:<,>\"]+").toString();
            Map<String, String> allPatternMap = Maps.newConcurrentMap();
            allPlaceholderMap.forEach((key, value) -> {
                String valuePattern = valuePatternModel.replace(PLACEHOLDER, key);
                allPatternMap.put(value, valuePattern);
            });
            allValueMap = PatternUtils.getMatcherValueMap(allPatternMap, srcStr, GroupName.REPLACE_KEY);
        }
        if (Objects.isNull(allValueMap)) {
            throw new CommonSystemServiceException(ErrorCodeEnum.ERROR_REQ_PARAM, "请求参数有误");
        }
        if (allValueMap.size() == 0) {
            return content;
        }

        //3.将占位符替换成值
        String result = StringUtils.replaceMap(content, allValueMap);
        return result;
    }

    private String getLocalMethodValue(String key) {
        if (StringUtils.isEmpty(key)) {
            throw new CommonSystemServiceException(ErrorCodeEnum.ERROR_RESPONSE_CONFIG, "响应配置有误");
        }
        ILocalMethod localMethod = null;
        String req = null;
        for (LocalMethodEnum localMethodEnum : LocalMethodEnum.values()) {
            if (key.startsWith(localMethodEnum.getCode())) {
                localMethod = localMethodEnum.getLocalMethod();
                req = PatternUtils.getValue(localMethodEnum.getPattern(), key, GroupName.LOCAL_METHOD_REQ);
            }
        }
        if (Objects.isNull(localMethod)) {
            throw new CommonSystemServiceException(ErrorCodeEnum.ERROR_RESPONSE_CONFIG, "响应配置有误");
        }
        if (StringUtils.isNotEmpty(req) && req.contains(",")) {
            req = req.substring(0, req.indexOf(","));
        }
        return (String) localMethod.invoke(req);
    }

    private Response getResponse(URI uri, String uriPath, String reqStr, ReqRegisterPo registerPo) {
        if (Objects.isNull(uri)) {
            throw new CommonSystemServiceException(ErrorCodeEnum.ERROR_NOT_FIND, "不支持的uri");

        }
        //1.获取serverCode
        SvrCode svrCode = uri.getSvrCode();
        PatternEnum patternEnum = svrCode.getPatternEnum();
        String serverCode = null;
        switch (patternEnum) {
            case REQUEST:
                serverCode = svrCode.getValue(reqStr);
                break;
            case HEADER:
                serverCode = svrCode.getValue(reqStr);
                break;
            case URI:
                serverCode = svrCode.getValue(uriPath);
                break;
        }
        if (StringUtils.isEmpty(serverCode)) {
            throw new CommonSystemServiceException(ErrorCodeEnum.ERROR_RESPONSE_CONFIG, "响应配置有误");
        }
        registerPo.setSvrCode(serverCode);

        //2.获取server
        List<Server> serverList = uri.getServerList();
        if (Objects.isNull(serverList) || serverList.size() == 0) {
            throw new CommonSystemServiceException(ErrorCodeEnum.ERROR_RESPONSE_CONFIG, "响应配置有误");
        }
        Server server = null;
        for (Server svr : serverList) {
            if (serverCode.equals(svr.getServerCode())) {
                server = svr;
            }
        }
        if (Objects.isNull(server)) {
            throw new CommonSystemServiceException(ErrorCodeEnum.ERROR_RESPONSE_CONFIG, "响应配置有误");
        }

        //3.获取 responseCode
        int responseCode;
        try {
            ResponseCode rspCode = server.getResponseCode();
            boolean hasCode = server.isHasCode();
            patternEnum = rspCode.getPatternEnum();
            switch (patternEnum) {
                case REQUEST:
                    responseCode = rspCode.getValue(reqStr, hasCode);
                    break;
                case URI:
                    responseCode = rspCode.getValue(uriPath, hasCode);
                    break;
                default:
                    throw new CommonSystemServiceException(ErrorCodeEnum.ERROR_RESPONSE_CONFIG, "响应配置有误");
            }
        } catch (Exception e) {
            log.error("获取responseCode失败,取默认值0,返回成功响应报文.", e);
            responseCode = 1;
        }


        //4.获取 response
        List<Response> responseList = server.getResponseList();
        if (Objects.isNull(responseList) || responseList.size() == 0) {
            throw new CommonSystemServiceException(ErrorCodeEnum.ERROR_RESPONSE_CONFIG, "响应配置有误");
        }
        Response response = null;
        for (Response rsp : responseList) {
            if (responseCode == rsp.getResponseCode()) {
                response = rsp;
            }
        }
        if (Objects.isNull(response)) {
            response = responseList.get(0);
        }
        return response;
    }
}
