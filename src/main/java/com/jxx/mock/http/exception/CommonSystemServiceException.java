/**
 * Bestpay.com.cn Inc.
 * Copyright (c) 2011-2018 All Rights Reserved.
 */
package com.jxx.mock.http.exception;

import com.jxx.mock.http.enums.ErrorCodeEnum;
import lombok.Getter;

/**
 * 统一的系统异常，不用更新入库状态
 *
 * @author jiangxingxing
 * @version Id: CommonSystemServiceException.java, v 0.1 2018/4/20 11:03 jiangxingxing Exp $$
 */
public class CommonSystemServiceException extends RuntimeException {

    private static final long serialVersionUID = -8998707909342242357L;

    @Getter
    private String errorCode = "";

    @Getter
    private String errorMsg = "";

    public CommonSystemServiceException(String errorCode, String errorMsg, Object... params) {
        super(new StringBuilder("[").append(errorCode).append("]").append(String.format(errorMsg, params)).toString());
        String format = String.format(errorMsg, params);
        this.errorCode = errorCode;
        this.errorMsg = format;
    }

    public CommonSystemServiceException(ErrorCodeEnum errorCodeEnum, String errorMsg, Object... params) {
        this(errorCodeEnum.getErrorCode(), errorMsg, params);
    }

    public CommonSystemServiceException(String errorCode, String errorMsg) {
        super(new StringBuilder("[").append(errorCode).append("]").append(errorMsg).toString());
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    public CommonSystemServiceException(ErrorCodeEnum errorCodeEnum, String errorMsg) {
        this(errorCodeEnum.getErrorCode(), errorMsg);
    }

    @Override
    public String getMessage() {
        return String.format("[%s]%s.", getErrorCode(), getErrorMsg());
    }
}