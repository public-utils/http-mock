package com.jxx.mock.http.config.entity;

import com.jxx.mock.http.enums.ResponseContentStatusEnum;
import com.jxx.mock.http.enums.ResponseContentTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * response xml解析对象
 *
 * @author xingxing jiang
 * @create 2021/06/29 14:39
 */
@Data
@AllArgsConstructor
@Builder
public class Response {
    private int id;
    private ResponseContentStatusEnum status;
    private ResponseContentTypeEnum type;
    private boolean model;
    private String modelPath;
    private String content;
    private int responseCode;
}
