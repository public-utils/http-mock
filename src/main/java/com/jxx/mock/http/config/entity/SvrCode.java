package com.jxx.mock.http.config.entity;

import com.jxx.mock.http.constant.GroupName;
import com.jxx.mock.http.enums.PatternEnum;
import com.jxx.mock.http.util.PatternUtils;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * svcCode xml解析对象
 *
 * @author xingxing jiang
 * @create 2021/06/29 14:34
 */
@Data
@AllArgsConstructor
@Builder
public class SvrCode {
    private PatternEnum patternEnum;
    private String pattern;

    public String getValue(String valueStr) {
        return PatternUtils.getValue(pattern, valueStr, GroupName.SVR_CODE);
    }
}
