package com.jxx.mock.http.config.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * server xml解析对象
 *
 * @author xingxing jiang
 * @create 2021/06/29 14:41
 */
@Data
@AllArgsConstructor
@Builder
public class Server {
    private int id;
    private String serverCode;
    private boolean hasCode;
    private ResponseCode responseCode;
    private List<Response> responseList;
}
