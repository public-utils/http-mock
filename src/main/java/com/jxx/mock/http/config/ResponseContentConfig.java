package com.jxx.mock.http.config;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jxx.mock.http.config.entity.*;
import com.jxx.mock.http.enums.ErrorCodeEnum;
import com.jxx.mock.http.enums.PatternEnum;
import com.jxx.mock.http.enums.ResponseContentStatusEnum;
import com.jxx.mock.http.enums.ResponseContentTypeEnum;
import com.jxx.mock.http.exception.CommonSystemServiceException;
import com.jxx.mock.http.util.EnumUtils;
import com.jxx.mock.http.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.annotation.PostConstruct;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author xingxing jiang
 * @create 2021/06/29 14:27
 */
@Slf4j
@Component
public class ResponseContentConfig {
    private Map<String, URI> config;

    @PostConstruct
    public void init() {
        config = Maps.newConcurrentMap();
        List<URI> uriList = parseConfig();
        initMap(uriList);
    }

    public URI getURI(String content) {
        if (Objects.isNull(content)) {
            log.error("响应配置有误，请检查后重启应用。。。。。");
            throw new CommonSystemServiceException(ErrorCodeEnum.ERROR_RESPONSE_CONFIG, "响应配置有误");
        }
        for (Map.Entry<String, URI> entry : config.entrySet()) {
            String uri = entry.getKey();
            if (content.startsWith(uri)) {
                return entry.getValue();
            }
        }
        return null;
    }

    private void initMap(List<URI> uriList) {
        if (Objects.isNull(uriList) || uriList.size() == 0) {
            log.error("响应配置有误，请检查后重启应用。。。。。");
            throw new CommonSystemServiceException(ErrorCodeEnum.ERROR_RESPONSE_CONFIG, "响应配置有误");
        }
        uriList.stream().forEach(uri -> config.put(uri.getContent().trim(), uri));
    }

    private List<URI> parseConfig() {
        //1.读取xml文件生成document
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        Document document = null;
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            String projectPath = this.getClass().getResource("/").toString();
            String configPath = new StringBuffer(projectPath).append("/config/ResponseContent.xml").toString();
            document = builder.parse(configPath);
            //2.解析uri list
            NodeList uriList = document.getElementsByTagName("uri");
            return element(uriList);
        } catch (Exception e) {
            log.error("响应配置有误", e);
            throw new CommonSystemServiceException(ErrorCodeEnum.ERROR_RESPONSE_CONFIG, "响应配置有误");
        }
    }

    private List<URI> element(NodeList uriList) throws IOException {
        if (Objects.isNull(uriList)) {
            throw new CommonSystemServiceException(ErrorCodeEnum.ERROR_RESPONSE_CONFIG, "响应配置有误");
        }
        List<URI> result = Lists.newArrayList();
        for (int i = 0; i < uriList.getLength(); i++) {
            Element uriElement = (Element) uriList.item(i);
            String uriId = uriElement.getAttribute("id");
            String uriContent = uriElement.getAttribute("content");

            //1.创建svrCode对象
            Element svrCodeElement = (Element) uriElement.getElementsByTagName("svrcode").item(0);
            String svrCodeContent = svrCodeElement.getTextContent().replaceAll("\\s", "");
            String svrCodePattern = svrCodeElement.getAttribute("pattern");
            PatternEnum svrCodePatternEnum = EnumUtils.nameOf(PatternEnum.class, svrCodePattern);
            SvrCode svrCode = SvrCode.builder().pattern(svrCodeContent).patternEnum(svrCodePatternEnum).build();

            //创建ServerList对象
            NodeList serverList = uriElement.getElementsByTagName("server");
            List<Server> svrList = Lists.newArrayList();
            for (int j = 0; j < serverList.getLength(); j++) {
                Element serverElement = (Element) serverList.item(j);
                String serverId = serverElement.getAttribute("id");
                String hasCodeStr = serverElement.getAttribute("hascode");
                String serverCode = serverElement.getAttribute("servercode");
                boolean hasCode = false;
                if (StringUtils.isNotEmpty(hasCodeStr)) {
                    hasCode = Boolean.valueOf(hasCodeStr);
                }


                //2.创建responseCode对象
                Element responseCodeElement = (Element) serverElement.getElementsByTagName("responsecode").item(0);
                String responseCodeContent = responseCodeElement.getTextContent().replaceAll("\\s", "");
                String responseCodePattern = responseCodeElement.getAttribute("pattern");
                PatternEnum responseCodePatternEnum = EnumUtils.nameOf(PatternEnum.class, responseCodePattern);
                ResponseCode responseCode = ResponseCode.builder().pattern(responseCodeContent).patternEnum(responseCodePatternEnum).build();

                //3.生成responseList对象
                NodeList responseList = serverElement.getElementsByTagName("response");
                List<Response> rspList = Lists.newArrayList();
                for (int k = 0; k < responseList.getLength(); k++) {
                    Element responseElement = (Element) responseList.item(k);
                    String responseId = responseElement.getAttribute("id");
                    String responseStatus = responseElement.getAttribute("status");
                    String code = responseElement.getAttribute("code");
                    String responseType = responseElement.getAttribute("type");
                    String responseModel = responseElement.getAttribute("model");
                    String responseContent = responseElement.getTextContent().replaceAll("\\s", "");
                    boolean model = Boolean.valueOf(responseModel);
                    ResponseContentStatusEnum responseStatusEnum = EnumUtils.nameOf(ResponseContentStatusEnum.class, responseStatus);
                    ResponseContentTypeEnum responseTypeEnum = EnumUtils.nameOf(ResponseContentTypeEnum.class, responseType);
                    String modelFile = "";
                    String content = responseContent;
                    switch (responseTypeEnum) {
                        case FILE:
                            modelFile = responseContent;
                            String projectPath = this.getClass().getResource("/").getPath();
                            modelFile = new StringBuffer(projectPath).append("/config/").append(modelFile).toString();
                            content = FileUtils.readFileToString(new File(modelFile));
                            break;
                        case CONTENT:
                            break;
                    }
                    int rspCode;
                    if (hasCode && StringUtils.isNotEmpty(code)) {
                        rspCode = Integer.valueOf(code);
                    } else {
                        rspCode = responseStatusEnum.getCode();
                    }
                    Response response = Response.builder().id(Integer.parseInt(responseId))
                            .status(responseStatusEnum)
                            .type(responseTypeEnum)
                            .model(model)
                            .content(content)
                            .modelPath(modelFile)
                            .responseCode(rspCode)
                            .build();
                    rspList.add(response);
                }

                //4.生成server对象
                Server server = Server.builder().id(Integer.parseInt(serverId))
                        .serverCode(serverCode)
                        .hasCode(hasCode)
                        .responseCode(responseCode)
                        .responseList(rspList)
                        .build();
                svrList.add(server);
            }
            //5.创建uri对象
            URI uri = URI.builder().id(Integer.parseInt(uriId))
                    .content(uriContent)
                    .svrCode(svrCode)
                    .serverList(svrList)
                    .build();
            result.add(uri);
        }
        return result;
    }


}
