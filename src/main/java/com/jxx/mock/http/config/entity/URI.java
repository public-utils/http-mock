package com.jxx.mock.http.config.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * uri xml解析对象
 *
 * @author xingxing jiang
 * @create 2021/06/29 14:41
 */
@Data
@AllArgsConstructor
@Builder
public class URI {
    private int id;
    private String content;
    private List<Server> serverList;
    private SvrCode svrCode;
}