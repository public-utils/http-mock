package com.jxx.mock.http.config.entity;

import com.jxx.mock.http.constant.GroupName;
import com.jxx.mock.http.enums.ErrorCodeEnum;
import com.jxx.mock.http.enums.PatternEnum;
import com.jxx.mock.http.exception.CommonSystemServiceException;
import com.jxx.mock.http.util.PatternUtils;
import com.jxx.mock.http.util.StringUtils;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * responseCode xml解析对象
 *
 * @author xingxing jiang
 * @create 2021/06/29 14:34
 */
@Data
@AllArgsConstructor
@Builder
public class ResponseCode {
    private PatternEnum patternEnum;
    private String pattern;

    public int getValue(String valueStr, boolean hasCode) {
        if (StringUtils.isEmpty(pattern)) {
            return 0;
        }
        String value = PatternUtils.getValue(pattern, valueStr, GroupName.RESPONSE_CODE);
        if (StringUtils.isEmpty(value)) {
            throw new CommonSystemServiceException(ErrorCodeEnum.ERROR_RESPONSE_CONFIG, "响应配置有误");
        }
        Integer code = Integer.valueOf(String.valueOf(value.charAt(value.length() - 1)));
        return hasCode ? code : code % 3;
    }
}
