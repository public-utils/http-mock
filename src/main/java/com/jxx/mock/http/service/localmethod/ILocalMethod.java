package com.jxx.mock.http.service.localmethod;

/**
 * 本地方法接口
 *
 * @author xingxing jiang
 * @create 2021/06/30 11:43
 */
public interface ILocalMethod<T, R> {
    T invoke(R req);
}