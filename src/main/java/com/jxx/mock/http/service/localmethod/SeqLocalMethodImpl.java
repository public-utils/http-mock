package com.jxx.mock.http.service.localmethod;

import com.jxx.mock.http.enums.ErrorCodeEnum;
import com.jxx.mock.http.exception.CommonSystemServiceException;
import com.jxx.mock.http.util.SequenceUtils;
import com.jxx.mock.http.util.StringUtils;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;

/**
 * 生成序列
 *
 * @author xingxing jiang
 * @create 2021/06/30 11:47
 */
@Slf4j
public class SeqLocalMethodImpl implements ILocalMethod<String, String> {
    @Override
    public String invoke(String req) {
        try {
            if (StringUtils.isEmpty(req)) {
                return SequenceUtils.flowNo();
            }
            int length = Integer.parseInt(req);
            return SequenceUtils.generateSeq(length);
        } catch (Exception e) {
            log.error("生成序列异常：", e);
            throw new CommonSystemServiceException(ErrorCodeEnum.ERROR_SEQ_FORMAT_CONFIG, "序列格式配置有误");
        }
    }
}