package com.jxx.mock.http.service.localmethod;

import com.jxx.mock.http.enums.ErrorCodeEnum;
import com.jxx.mock.http.exception.CommonSystemServiceException;
import com.jxx.mock.http.util.DateTimeUtils;
import com.jxx.mock.http.util.StringUtils;

/**
 * 生成日期
 *
 * @author xingxing jiang
 * @create 2021/06/30 11:47
 */
public class DateLocalMethodImpl implements ILocalMethod<String, String> {
    @Override
    public String invoke(String req) {
        try {
            if (StringUtils.isEmpty(req)) {
                return DateTimeUtils.defaultDatetime();
            }
            return DateTimeUtils.formatDatetime(req);
        } catch (Exception e) {
            throw new CommonSystemServiceException(ErrorCodeEnum.ERROR_DATE_FORMAT_CONFIG, "日期格式配置有误");
        }
    }
}