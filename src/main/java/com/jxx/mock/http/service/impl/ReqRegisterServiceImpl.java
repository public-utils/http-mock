package com.jxx.mock.http.service.impl;

import com.jxx.mock.http.entity.po.ReqRegisterPo;
import com.jxx.mock.http.repository.ReqRegisterRepository;
import com.jxx.mock.http.service.IReqRegisterService;
import org.springframework.stereotype.Service;

/**
 * http mock请求登记簿服务实现
 *
 * @author xingxing jiang
 * @create 2021/06/28 14:06
 */
@Service
public class ReqRegisterServiceImpl implements IReqRegisterService {
    private final ReqRegisterRepository repository;

    public ReqRegisterServiceImpl(ReqRegisterRepository repository) {
        this.repository = repository;
    }


    @Override
    public ReqRegisterPo insert(ReqRegisterPo entity) {
        return repository.save(entity);
    }

    @Override
    public ReqRegisterPo queryById(String id) {
        return repository.getOne(id);
    }
}
