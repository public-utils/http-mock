package com.jxx.mock.http.service.impl;

import com.jxx.mock.http.constant.AppConstant;
import com.jxx.mock.http.entity.po.ReqRegisterPo;
import com.jxx.mock.http.queue.service.ITaskService;
import com.jxx.mock.http.queue.service.TaskServiceImpl;
import com.jxx.mock.http.repository.ReqRegisterRepository;
import com.jxx.mock.http.service.IReqRegisterService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * http mock请求登记簿服务实现 异步队列
 *
 * @author xingxing jiang
 * @create 2021/06/28 14:06
 */
@Slf4j
@Service(value = "reqRegisterServiceQueue")
public class ReqRegisterServiceQueueImpl implements IReqRegisterService {
    private final ReqRegisterRepository repository;

    public ReqRegisterServiceQueueImpl(ReqRegisterRepository repository) {
        this.repository = repository;
    }

    private ITaskService<ReqRegisterPo> insertTaskService;

    @PostConstruct
    public void init() {
        insertTaskService = new TaskServiceImpl<>(t -> repository.save(t));
    }

    @PreDestroy
    public void destroy() {
        insertTaskService.destroy();
    }

    @Override
    public ReqRegisterPo insert(ReqRegisterPo entity) {
        return insertTaskService.submit(MDC.get(AppConstant.LOG_SEQ), entity) ? entity : null;
    }

    @Override
    public ReqRegisterPo queryById(String id) {
        return repository.getOne(id);
    }
}