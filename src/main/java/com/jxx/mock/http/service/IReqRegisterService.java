package com.jxx.mock.http.service;

import com.jxx.mock.http.entity.po.ReqRegisterPo;

/**
 * http mock请求登记簿服务
 *
 * @author xingxing jiang
 * @create 2021/06/28 14:05
 */
public interface IReqRegisterService {
    /**
     * 插入记录
     *
     * @param entity 实体对象
     * @return
     */
    ReqRegisterPo insert(ReqRegisterPo entity);

    /**
     * 查询记录
     *
     * @param id 主键id
     * @return
     */
    ReqRegisterPo queryById(String id);
}