package com.jxx.mock.http.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * mock响应数据配置的类型
 *
 * @author xingxing jiang
 * @create 2021/06/29 13:06
 */
@Getter
@AllArgsConstructor
public enum ResponseContentTypeEnum {
    FILE("FILE"),
    CONTENT("CONTENT");
    private String code;
}