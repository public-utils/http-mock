package com.jxx.mock.http.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * mock的响应数据状态
 *
 * @author xingxing jiang
 * @create 2021/06/29 13:06
 */
@Getter
@AllArgsConstructor
public enum ResponseContentStatusEnum {
    SUCCESS(0),
    FAIL(1),
    EXCEPTION(2);
    private int code;
}