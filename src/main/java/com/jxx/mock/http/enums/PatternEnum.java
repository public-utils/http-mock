package com.jxx.mock.http.enums;

/**
 * svrCode正则配置的配置值
 *
 * @author xingxing jiang
 * @create 2021/06/29 14:34
 */
public enum PatternEnum {
    METHOD,
    REQUEST,
    HEADER,
    URI;
}