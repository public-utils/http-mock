package com.jxx.mock.http.enums;


import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * http响应码
 *
 * @author xingxing jiang
 * @create 2021/06/28 11:00
 */
@Getter
@AllArgsConstructor
public enum HttpResponseStatusEnum {
    SUCCESS("200", "success"),
    NO_SUPPORT_METHOD("405", "no support method"),
    NO_FOUND("404", "no found"),
    INTERNAL_EXCEPTION("500", "server internal exception");
    private String code;
    private String msg;
}