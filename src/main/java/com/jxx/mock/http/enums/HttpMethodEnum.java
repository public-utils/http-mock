package com.jxx.mock.http.enums;


import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * http请求的方法枚举
 *
 * @author xingxing jiang
 * @create 2021/06/28 11:00
 */
@Getter
@AllArgsConstructor
public enum HttpMethodEnum {
    GET(1),
    POST(2),
    DELETE(4),
    HEAD(5),
    CONNECT(6),
    OPTIONS(7),
    PATCH(8),
    TRACE(9);
    private int code;
}