/**
 * Bestpay.com.cn Inc.
 * Copyright (c) 2011-2018 All Rights Reserved.
 */
package com.jxx.mock.http.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 错误码枚举
 *
 * @author xingxing jiang
 * @create 2021/06/28 11:00
 */
@Getter
@AllArgsConstructor
public enum ErrorCodeEnum {

    /*成功代码*/
    SUCCESS("0000"),

    /*系统类异常*/
    ERROR_SYSTEM_UNKNOWN("9999"),
    ERROR_NO_SERVICE("4001"),
    ERROR_RESPONSE_CONFIG("4002"),
    ERROR_DATE_FORMAT_CONFIG("4003"),
    ERROR_SEQ_FORMAT_CONFIG("4004"),
    ERROR_NOT_FIND("4005"),

    /*业务类异常（不用监控）*/
    ERROR_SVR_CODE("1001"),
    ERROR_INVOKE_METHOD("1002"),
    ERROR_REQ_PARAM("1009");

    private String errorCode;
}