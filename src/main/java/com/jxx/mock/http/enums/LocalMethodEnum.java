package com.jxx.mock.http.enums;

import com.jxx.mock.http.service.localmethod.DateLocalMethodImpl;
import com.jxx.mock.http.service.localmethod.ILocalMethod;
import com.jxx.mock.http.service.localmethod.SeqLocalMethodImpl;
import lombok.AllArgsConstructor;
import lombok.Getter;


/**
 * 本地方法枚举
 *
 * @author xingxing jiang
 * @create 2021/06/30 11:39
 */
@Getter
@AllArgsConstructor
public enum LocalMethodEnum {
    DATE("date", "date\\((?<localMethodReq>.*?)\\)", new DateLocalMethodImpl()),
    SEQ("seq", "seq\\((?<localMethodReq>\\d*?)\\)", new SeqLocalMethodImpl());
    private String code;
    private String pattern;
    private ILocalMethod localMethod;
}