package com.jxx.mock.http.entity.po;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * http请求登记簿对象实体类
 *
 * @author xingxing jiang
 * @create 2021/06/28 12:03:43
 */
@Entity
@Table(name = "HTTP_MOCK_FLOW")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ReqRegisterPo {
    /**
     * 请求订单id
     */
    @Id
    private String orderId;
    /**
     * 原请求订单id
     */
    @Column
    private String origOrderId;
    /**
     * 通道编码
     */
    @Column
    private String channelCode;
    /**
     * 请求url
     */
    @Column
    private String uri;
    /**
     * 请求服务代码
     */
    @Column
    private String svrCode;
    /**
     * 请求方法，枚举类0:POST,1:GET等
     */
    @Column
    private int httpMethod;
    /**
     * 请求参数
     */
    @Column
    private String reqPram;
    /**
     * 响应参数
     */
    @Column
    private String respPram;
    /**
     * 响应码
     */
    @Column
    private String respCode;
    /**
     * 响应描述
     */
    @Column
    private String respMsg;
    /**
     * 插入时间
     */
    @Column
    @CreationTimestamp
    private Date createTime;
}