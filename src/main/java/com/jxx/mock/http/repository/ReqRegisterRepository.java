package com.jxx.mock.http.repository;

import com.jxx.mock.http.entity.po.ReqRegisterPo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author xingxing jiang
 * @create 2021/06/28 13:25
 */
@Repository
public interface ReqRegisterRepository extends JpaRepository<ReqRegisterPo, String> {
}