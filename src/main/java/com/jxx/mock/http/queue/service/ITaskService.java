package com.jxx.mock.http.queue.service;

/**
 * 提供队列服务
 *
 * @author xingxing jiang
 * @create 2021/07/02 10:20
 */
public interface ITaskService<T> {
    /**
     * 提交任务
     *
     * @param logSeq 日志流水
     * @param task   任务对象
     */
    boolean submit(String logSeq, T task);

    /**
     * 停止任务
     */
    void destroy();
}