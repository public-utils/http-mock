package com.jxx.mock.http.queue.task;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * 任务对象
 *
 * @author xingxing jiang
 * @create 2021/07/02 10:16
 */
@Data
@AllArgsConstructor
@Builder
public class MyTask<T> {
    private T task;
    String logSeq;
}