package com.jxx.mock.http.queue.service;

import com.jxx.mock.http.constant.AppConstant;
import com.jxx.mock.http.queue.task.MyTask;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Consumer;

/**
 * @author xingxing jiang
 * @create 2021/07/02 10:23
 */
@Slf4j
public class TaskServiceImpl<T> implements ITaskService<T> {

    private final LinkedBlockingQueue<MyTask<T>> tasks;

    private ExecutorService service;

    private boolean isRunning = true;

    public TaskServiceImpl(Consumer<T> consumer) {
        tasks = new LinkedBlockingQueue<MyTask<T>>(5000);
        service = Executors.newSingleThreadExecutor();
        service.submit(() -> {
            while (isRunning) {
                try {
                    MyTask<T> take = tasks.take();
                    String logSeq = take.getLogSeq();
                    MDC.put(AppConstant.LOG_SEQ, logSeq);
                    Thread.sleep(10000);
                    consumer.accept(take.getTask());
                } catch (InterruptedException e) {
                    log.error("任务处理发生异常", e);
                }
            }
        });
    }

    @Override
    public boolean submit(String logSeq, T task) {
        MyTask<T> myTask = (MyTask<T>) MyTask.builder().logSeq(logSeq).task(task).build();
        boolean offer = tasks.offer(myTask);
        if (!offer) {
            log.warn("任务添加到队列失败");
        }
        return offer;
    }

    @Override
    public void destroy() {
        this.isRunning = false;
        service.shutdownNow();
        tasks.clear();
    }
}