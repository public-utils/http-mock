package com.jxx.mock.http.constant;

/**
 * 正则分组名称
 *
 * @author xingxing jiang
 * @create 2021/06/29 15:44
 */
public interface GroupName {
    String SVR_CODE = "svrCode";
    String RESPONSE_CODE = "responseCode";
    String REPLACE_KEY = "replaceKey";
    String LOCAL_METHOD_REQ = "localMethodReq";
}
