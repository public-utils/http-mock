package com.jxx.mock.http.constant;

/**
 * @author xingxing jiang
 * @create 2021/07/02 10:37
 */
public interface AppConstant {

    /**
     * 日志流水标识
     */
    String LOG_SEQ = "logSeq";
}
